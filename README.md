# mlops_project
## Description
В данное время проект не выбран и его описание появится позже.


# Frameworks/Libraries/Tools
 - PDM
 - pytorch

# How to manage with project:
 [see CONTRIBUTING.md](CONTRIBUTING.md)

# Project configuration


    ├── README.md               <- The top-level README for developers using this project.
    ├── CONTRIBUTING.md         <- Description of the project, its goals and basic principles of development.
    ├── data
    │   ├── external            <- Data from third party sources.
    │   ├── interim             <- Intermediate data that has been transformed.
    │   ├── processed           <- The final, canonical data sets for modeling.
    │   └── raw                 <- The original, immutable data dump.
    │
    ├── models                  <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks               <- Jupyter notebooks fir analysis
    │
    ├── references              <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── src                     <- Source code for use in this project.
    │   ├── __init__.py         <- Makes src a Python module
    │   │
    │   ├── data                <- Scripts to download or generate data
    │   │
    │   └── models              <- Scripts to train models and then use trained models to make
    │       │                      predictions
    │       ├── predict_model.py
    │       └── train_model.py
    │   
    │
    ├── pyproject.toml   
    ├── .gitignore  
    ├── .pre-commit-config.yaml 
    └── pdm.lock                <-  Exact versions of all project's dependencies



