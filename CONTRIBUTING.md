# Contributing to Test mlops Project

Thank you for considering contributing to Test Project! Your help is greatly appreciated.

## About the Project

Test Project is an open-source Python project developed using the PDM file manager. It aims to explore various topics in the field of Data Science, although the specific theme has not been chosen yet.

## Getting Started

To contribute to Test Project, follow these steps:
1. Fork the repository on GitLab.
2. Clone your fork of the repository to your local machine:

```sh
git clone https://gitlab.com/lapki1/test_project.git
cd test_project
```

3. Install dependencies using PDM:
```sh
pdm install
```
4. (Optional) Set up a virtual environment using PDM:
```sh
pdm use
```


## Code Formatting

We use several tools for code formatting and linting:
- [ruff](https://docs.astral.sh/ruff/) for code formatting
- [mypy](https://mypy-lang.org/) for static type checking
- [pycln](https://pypi.org/project/pycln/1.2.3/) for code linting

These tools are configured to run automatically using pre-commit hooks. Please make sure to install them before contributing:
```sh
pdm install ruff mypy pycln
```

## Reporting Issues

If you find a bug or have a suggestion for improvement, please open a new issue on the GitLab repository. Be sure to include a detailed description of the issue and steps to reproduce it.

## Making Changes

To make changes to the project, follow these steps:
1. Create a new branch for your changes:
```sh
git checkout -b my-feature-branch
```
2. Make your changes and commit them:
```sh
git commit -am "Add new feature"
```
3. Push your changes to your forked repository:
```sh
git push origin my-feature-branch
```
4. Create a new merge request (MR) on GitLab with your changes.

## Code Review

All contributions to Test Project are reviewed before they are merged. Be prepared to address any feedback received during the review process.







